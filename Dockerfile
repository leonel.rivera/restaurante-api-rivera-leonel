FROM node:16.1.0-alpine3.13
ENV NODE_ENV=production

RUN apk add tzdata
RUN cp /usr/share/zoneinfo/America/Argentina/Ushuaia /etc/localtime
RUN echo "America/Argentina/Ushuaia" >  /etc/timezone

RUN mkdir /Restaurante-api
WORKDIR /Restaurante-api

RUN npm i -g nodemon

EXPOSE ${PORT_BACK}

CMD ["npm", "start"]