# restaurante-api-rivera-leonel

#restaurant api

Proyecto Sprint 4 - Desarrollo Web Back End

************\*\*\*\************* Instrucciones de Instalación ************\*\*\*\*************

git clone https://gitlab.com/leonel.rivera/restaurante-api-rivera-leonel

cd restaurante-api-rivera-leonel

renombrar archivo .env_example a .env, se encuentra en el directorio raiz. En caso de ser necesario modificar alguna variable de entorno.

Depende de lo utilizado en este punto, npm install o docker-compose run --rm back npm i, es como se ejecutara el proyecto.

************\*\*\*\************* BASE DE DATOS ************\*\*\*\*************

Importar BBDD en su administrador de base de datos.

cd restaurante-api-rivera-leonel/database/bbdd.sql

************\*\*\*\************* Correr el proyecto ************\*\*\*\*************

De acuerdo como se procedio en la instalacion se debe ejecutar: node index.js o docker-compose up -d

************\*\*\*\************* URl del proyecto ************\*\*\*\*************

Navegador url: http://localhost:4000/api-docs/

************\*\*\*\************* Usuarios precargados ************\*\*\*\*************

Usuarios cargados:
USUARIO CONTRASEÑA ROL
admin admin admin
user user user

************\*\*\*\************* Correr testing proyecto ************\*\*\*\*************

npm test
