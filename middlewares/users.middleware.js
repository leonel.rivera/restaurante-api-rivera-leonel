require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const Sequelize = require("sequelize");
const sequelize = require("../config/db.config");
const { Op } = require("sequelize");
const User = require("../models/users.model");
const Role = require("../models/roles.model");
const userController = require("../controllers/users.controller");
//SessionStorage
const sessionStorage = require("sessionstorage-for-nodejs");

const validateLogin = async (req, res, next) => {
  const prevState = await User.findOne({
    where: {
      state_user: true,
    },
  });

  const validate = await User.findOne({
    where: {
      active_user: true,
      [Op.or]: [
        { username_user: req.body.username },
        { email_user: req.body.username },
      ],
    },
  });

  bcrypt.compare(
    req.body.password,
    validate.password_user,
    function (err, result) {
      if (err) {
        res.json("Error checking password");
      }
      if (result) {
        if (validate == null) {
          res.json("check username or password or disabled user");
        } else if (validate.state_user == true) {
          res.json("you are logged in");
        } else if (prevState == null) {
          User.update(
            {
              state_user: true,
            },
            { where: { id_user: validate.id_user } }
          );
          const token = jwt.sign(
            {
              id_user: validate.id_user,
              username_user: validate.username_user,
              password_user: validate.password_user,
            },
            process.env.JWT_SECRET,
            { expiresIn: "1d" }
          );
          /* console.log("token " + token);
          SessionStorage.setItem("token", token); */
          User.token = token;

          res.json(/* "Logged In - Copy TOKEN " + */ token);
          next();
        } else {
          res.json(`Session started `);
        }
      } else {
        res.json("Incorrect password");
      }
    }
  );
};

const validateLoginidp = async (req, res, next) => {
  const state = await User.findOne({
    where: {
      state_user: true,
    },
  }).then((user) => {
    if (user == null) {
      res.json("No user logged in");
      console.log("if then user " + user);
    } else {
      //res.json("User logged in");
      console.log("else then user " + JSON.stringify(user));
      const token = jwt.sign(
        {
          id_user: user.id_user,
          username_user: user.username_user,
          password_user: user.password_user,
        },
        process.env.JWT_SECRET,
        { expiresIn: "1d" }
      );
      console.log("token " + token);
      //SessionStorage.setItem("token", token);

      User.token = token;

      res.status(301).redirect(process.env.API_FRONT);
      //res.json(/* "Logged In - Copy TOKEN " + */ token);
      next();
    }
  });

  /* console.log("state " + JSON.parse(state));

  if (state.id_idp !== null) {
    const token = jwt.sign(
      {
        id_user: state.id_user,
        username_user: state.username_user,
        password_user: state.password_user,
      },
      process.env.JWT_SECRET,
      { expiresIn: "1d" }
    ); */
  /* console.log("token " + token);
    SessionStorage.setItem("token", token); */
  //User.token = token;

  //res.json(/* "Logged In - Copy TOKEN " + */ token);
  //SessionStorage.setItem("jwt", token);
  //next();
  //};
};

const validateLogout = async (req, res, next) => {
  const validate = await User.findOne({
    where: {
      state_user: true,
    },
  });

  if (validate == null) {
    res.json("no logged in users");
  } else {
    User.update(
      {
        state_user: false,
      },
      { where: { id_user: validate.id_user } }
    );
    User.token = "";
    res.json("Logged Out");
    next();
  }
};

const loginSate = async (req, res, next) => {
  const index = await User.findOne({
    where: {
      state_user: true,
    },
  });
  console.log("index " + index);
  if (
    index !== null ||
    req.path == "/users/login" ||
    req.path == "/users/register" ||
    req.path == "/users/google" ||
    req.path == "/users/google/redirect" ||
    req.path == "/users/facebook" ||
    req.path == "/users/facebook/redirect" ||
    req.path == "/users/linkedin" ||
    req.path == "/users/linkedin/redirect"
  ) {
    next();
  } else {
    res.json("Not logged in");
  }
};

const loginRol = async (req, res, next) => {
  let msj = {};
  const index = await User.findOne({
    where: {
      id_role: 1,
      state_user: true,
    },
  });
  const path = await User.findOne({
    where: {
      id_role: 2,
      state_user: true,
    },
  });
  const str = req.path;
  const stringdelete = str.substring(17, str.length);
  const paramsId = req.path.replace(stringdelete, "");

  console.log(
    "path " +
      path +
      " index " +
      index +
      " req.path " +
      req.path +
      " req.params.id " +
      ` /users/modificar/${paramsId} ` +
      " stringdelete " +
      stringdelete +
      " paramsId " +
      paramsId
  );

  if (index !== null) {
    next();
  } else if (
    path !== null &&
    (req.path == "/orders/delete" ||
      req.path == "/orders/put/admin" ||
      /* req.path == "/products" || */
      req.path == "/users" ||
      req.path == `/users/modificar/${stringdelete}` ||
      /* req.path == "/payments" || */
      req.path == "/orders/get/admin")
  ) {
    res.status(401).send({
      status: 401,
      message: "Does not have authorization",
    });
  } else {
    next();
  }
};

const verifyToken = async (req, res, next) => {
  console.log("req.headers.authorization " + req.headers.authorization);
  console.log("user token " + User.token);
  if (
    req.path == "/users/login" ||
    req.path == "/users/register" ||
    req.path == "/users/google" ||
    req.path == "/users/google/redirect" ||
    req.path == "/users/facebook" ||
    req.path == "/users/facebook/redirect" ||
    req.path == "/users/linkedin" ||
    req.path == "/users/linkedin/redirect"
  ) {
    next();
  } else if (
    typeof req.headers.authorization !== undefined ||
    User.token !== undefined
  ) {
    let token = "";
    User.token !== "undefined"
      ? (token = User.token)
      : (token = req.headers.authorization.replace("Bearer ", ""));
    if (token === "") res.json("Error token vacio");
    const decoded = jwt.verify(
      token,
      process.env.JWT_SECRET,
      function (err, decoded) {
        if (err) {
          console.log({ decoded, err });
          if (err.name == "TokenExpiredError") {
            User.update(
              {
                state_user: false,
              },
              { where: { state_user: true } }
            );
            res.json("Login again");
          } else {
            User.update(
              {
                state_user: false,
              },
              { where: { state_user: true } }
            );
            res.send("Invalid Token. Login again");
          }
        } else {
          next();
        }
      }
    );
  } else {
    res.status(404).json("Error token vacio");
  }
};

const verifyState = async (req, res, next) => {
  const verify = await User.findOne({
    where: {
      state_user: true,
    },
  });
  if (verify !== null) {
    User.update(
      {
        state_user: false,
      },
      { where: { state_user: true } }
    );
  } else {
    next();
  }
};

//cors y cabeceras
const cors = async (req, res, next) => {
  // Dominio que tengan acceso (ej. 'http://example.com')
  //res.setHeader("Access-Control-Allow-Origin", "*");

  // Metodos de solicitud que deseas permitir
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");

  // Encabecedados que permites (ej. 'X-Requested-With,content-type')
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  console.log("entre en cors");

  /* res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true); */

  /* res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE"); */
  next();
};

module.exports = {
  validateLogin,
  validateLoginidp,
  validateLogout,
  loginSate,
  loginRol,
  verifyToken,
  verifyState,
  cors,
};
