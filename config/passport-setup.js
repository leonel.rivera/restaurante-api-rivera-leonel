require("dotenv").config();
const { Router } = require("express");
const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;
const LinkedInStrategy = require("passport-linkedin-oauth2").Strategy;
const User = require("../models/users.model");

passport.serializeUser((user, done) => {
  console.log("serializeUser" + user);
  done(null, user.id_user);
});

passport.deserializeUser((id_user, done) => {
  console.log("deserializeUser" + id_user);
  User.findById(id_user).then((user) => {
    console.log("deserializeUser" + user);
    done(null, user);
  });
});

passport.use(
  new GoogleStrategy(
    {
      // options for google strategy
      callbackURL: process.env.API + "users/google/redirect",
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    },
    (accessToken, refreshToken, profile, done) => {
      // callback function
      console.log("funcion de redirect google");
      console.log(profile);

      User.findOne({
        where: {
          id_idp: profile.id,
        },
      }).then((currentUser) => {
        if (currentUser) {
          console.log("user already exists");
          console.log(JSON.stringify(currentUser));
          User.update(
            {
              state_user: true,
            },
            {
              where: {
                id_user: currentUser.id_user,
              },
            }
          ).then((user) => {
            console.log("user updated");
            console.log(JSON.stringify(user));
            done(null, currentUser);
          });
          //done(null, currentUser);
        } else {
          console.log("user not exists");
          User.build({
            username_user: profile.displayName,
            state_user: true,
            active_user: true,
            id_role: 2,
            id_idp: profile.id,
          })
            .save()
            .then((user) => {
              console.log("user created idp" + user);
              console.log(JSON.stringify(user));
              done(null, user);
            });
        }
      });
    }
  )
);

/* passport.use(
  new FacebookStrategy(
    {
      // options for facebook strategy
      callbackURL: process.env.API + "users/facebook/redirect",
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    },
    (accessToken, refreshToken, profile, done) => {
      // callback function
      console.log("funcion de redirect facebook");
      console.log(profile);

      User.findOne({
        where: {
          id_idp: profile.id,
        },
      }).then((currentUser) => {
        if (currentUser) {
          console.log("user already exists");
          console.log(JSON.stringify(currentUser));
          User.update(
            {
              state_user: true,
            },
            {
              where: {
                id_user: currentUser.id_user,
              },
            }
          ).then((user) => {
            console.log("user updated");
            console.log(JSON.stringify(user));
            done(null, currentUser);
          });
          //done(null, currentUser);
        } else {
          console.log("user not exists");
          User.build({
            username_user: profile.displayName,
            state_user: true,
            active_user: true,
            id_role: 2,
            id_idp: profile.id,
          })
            .save()
            .then((user) => {
              console.log("user created idp" + user);
              console.log(JSON.stringify(user));
              done(null, user);
            });
        }
      });
    }
  )
); */

passport.use(
  new LinkedInStrategy(
    {
      // options for linkedin strategy
      callbackURL: process.env.API + "users/linkedin/redirect",
      clientID: process.env.LINKEDIN_CLIENT_ID,
      clientSecret: process.env.LINKEDIN_CLIENT_SECRET,
    },
    (accessToken, refreshToken, profile, done) => {
      // callback function
      console.log("funcion de redirect linkedin");
      console.log(profile);

      User.findOne({
        where: {
          id_idp: profile.id,
        },
      }).then((currentUser) => {
        if (currentUser) {
          console.log("user already exists");
          console.log(JSON.stringify(currentUser));
          User.update(
            {
              state_user: true,
            },
            {
              where: {
                id_user: currentUser.id_user,
              },
            }
          ).then((user) => {
            console.log("user updated");
            console.log(JSON.stringify(user));
            done(null, currentUser);
          });
          //done(null, currentUser);
        } else {
          console.log("user not exists");
          User.build({
            username_user: profile.displayName,
            state_user: true,
            active_user: true,
            id_role: 2,
            id_idp: profile.id,
          })
            .save()
            .then((user) => {
              console.log("user created idp" + user);
              console.log(JSON.stringify(user));
              done(null, user);
            });
        }
      });
    }
  )
);
