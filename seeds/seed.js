const sequelize = require('../config/db.config');
const User = require('../models/users.model');
const State = require('../models/states.model');
const Role = require('../models/roles.model');
const Product = require('../models/products.model');
const Payment = require('../models/payments.model');
const Order = require('../models/orders.model');
const Address = require('../models/addresses.model');
require('../models/asociations');

// Roles
const roles = [
    { name_role: "admin"},
    { name_role: "user"}
];

// Users
const users = [
    { username_user: "admin", namelastname_user: "admin", email_user: "admin@mail.com", phone_user: "123456",address_user: "domicilio admin ",password_user:'$2b$10$A2SnBwoMvvWlOkpUeY1aqeh30hDSpoNO2Rw7ZZOBoytuVPw9mu9BS',state_user:0,active_user:1,id_role:1,id_idp:"" },
    { username_user: "user", namelastname_user: "user", email_user: "user@mail.com", phone_user: "123456",address_user: "domicilio user ",password_user:'$2b$10$MFWHZ4GEooa28NJ4MmvCdeIo9JNmbn9ed5SyzCsI6niR3eqNtFhn6',state_user:0,active_user:1,id_role:2,id_idp:"" }
];

// States
const states = [
    { name_state: "Nuevo"},
    { name_state: "Confirmado"},
    { name_state: "Preparando"},
    { name_state: "Enviando"},
    { name_state: "Cancelado"},
    { name_state: "Entregado"},
];


// Payments
const payments = [
    { name_payment: "Efectivo"},
    { name_payment: "Tarjeta de Debito"},
    { name_payment: "Tarjeta de Credito"},
    { name_payment: "Transferencia Bancaria"}
];

// Products
const products = [
    { name_product: "Hamburguesa simple",price_product:6},
    { name_product: "Hamburguesa Doble",price_product:8},
    { name_product: "Pancho",price_product:3}
];


sequelize.sync({ force: true }).then(() => {
    // Established connection 
    console.log("Established connection...");
}).then(() => {
    // Fill states
    states.forEach(state => State.create(state));
}).then(() => {
    // Fill roles
    roles.forEach(role => Role.create(role));
}).then(() => {
    // Fill payment
    payments.forEach(payment => Payment.create(payment));
}).then(() => {
    // Fill products
    products.forEach(product => Product.create(product));
}).then(() => {
    // Fill usuarios
    users.forEach(user => User.create(user));
});