require("dotenv").config();
const Role = require('../models/roles.model');


const addRole = async (req) => {
    const newRole = await Role.build({
        name_role: req.body.name_role
    });

    const result = await newRole.save();
    return result;
}

const listRole = async () => await Role.findAll();

const modifyRole = async (req) => {
    const id_role = parseInt(req.params.id);
    const result = await Role.update({
        name_role: req.body.name_role
    },
    { where: { id_role: id_role } }
    );
    return result;
}

const deleteRole = async (req) => {
    const id_role = parseInt(req.params.id);
    const result = await Role.destroy({
        where: { id_role: id_role }
    });
    return result;
}

module.exports = {
    listRole,
    addRole,
    modifyRole,
    deleteRole
}
