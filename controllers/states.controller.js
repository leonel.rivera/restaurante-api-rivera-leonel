require("dotenv").config();
const State = require('../models/states.model');


const addState = async (req) => {
    const newState = await State.build({
        name_state: req.body.name_state
    });

    const result = await newState.save();
    return result;
}

const listState = async () => await State.findAll();

const modifyState = async (req) => {
    const id_state = parseInt(req.params.id);
    const result = await State.update({
        name_state: req.body.name_state
    },
    { where: { id_state: id_state } }
    );
    return result;
}

const deleteState = async (req) => {
    const id_state = parseInt(req.params.id);
    const result = await State.destroy({
        where: { id_state: id_state }
    });
    return result;
}

module.exports = {
    listState,
    addState,
    modifyState,
    deleteState
}
