require("dotenv").config();
const Payment = require("../models/payments.model");

const addPayment = async (req) => {
  const newPayment = await Payment.build({
    name_payment: req.body.name_payment,
  });

  const result = await newPayment.save();
  return result;
};

const listPayment = async () => await Payment.findAll();

const modifyPayment = async (req) => {
  const id_payment = parseInt(req.params.id);
  console.log("modify payment " + req.body.name_payment);
  const result = await Payment.update(
    {
      name_payment: req.body.name_payment,
    },
    { where: { id_payment: id_payment } }
  );
  return result;
};

const deletePayment = async (req) => {
  const id_payment = parseInt(req.params.id);
  const result = await Payment.destroy({
    where: { id_payment: id_payment },
  });
  return result;
};

module.exports = {
  listPayment,
  addPayment,
  modifyPayment,
  deletePayment,
};
