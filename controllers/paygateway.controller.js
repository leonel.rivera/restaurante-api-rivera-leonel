require("dotenv").config();
// SDK de Mercado Pago
const mercadopago = require("mercadopago");

// Agrega credenciales
mercadopago.configure({
  access_token: process.env.MERCADO_ACCESS_TOKEN,
});

const createPay = async (req, res) => {
  // Crea un objeto de preferencia
  const preference = {
    items: [
      {
        //id: req.body.id_payment,
        title: "Pago de productos",
        description: "Pago de productos",
        quantity: 1,
        unit_price: 150,
      },
    ],
    /* payer: {
      name: req.body.name_payment,
      surname: req.body.surname_payment,
      email: req.body.email_payment,
      phone: {
        area_code: req.body.area_code_payment,
        number: req.body.phone_payment,
      },
      address: {
        street_name: req.body.street_name_payment,
        street_number: req.body.street_number_payment,
        zip_code: req.body.zip_code_payment,
        city: req.body.city_payment,
        state: req.body.state_payment,
        country: req.body.country_payment,
      },
    }, */
    back_urls: {
      success: "/",
      failure: "/",
      /* success: process.env.BACK_URL_SUCCESS,
      failure: process.env.BACK_URL_FAILURE, */
    },
    auto_return: "approved",
    //external_reference: req.body.id_payment,
  };

  mercadopago.preferences
    .create(preference)
    .then(function (response) {
      // En esta instancia deberás asignar el valor dentro de response.body.id por el ID de preferencia solicitado en el siguiente paso
      console.log(response.body);
    })
    .catch(function (error) {
      console.log(error);
    });

  return preference;

  //return result;
};

module.exports = {
  createPay,
};
