require("dotenv").config();
const Product = require("../models/products.model");
const redis = require("redis");
const bluebird = require("bluebird");
bluebird.promisifyAll(redis);
console.log(process.env.ELASTICACHE_URL);
const client = redis.createClient({
  host: process.env.ELASTICACHE_URL,
  port: 6379,
});
//const client = redis.createClient();

const addProduct = async (req) => {
  const newProduct = await Product.build({
    name_product: req.body.name,
    price_product: req.body.price,
  });

  const result = await newProduct.save();
  return result;
};

const listProduct = async (products) => {
  const productsOnRedis = await client.getAsync(products);

  if (productsOnRedis !== null) {
    console.log("esta en cache");
    return JSON.parse(productsOnRedis);
  } else {
    const result = Product.findAll()
      .then((res) => {
        console.log("guardo en cache");
        client.set(products, JSON.stringify(res), "EX", 60 * 60 * 12);
        return res;
      })
      .catch((err) => {
        return err;
      });
    return result;
  }
};

const modifyProduct = async (req, products) => {
  console.log(
    "modificar producto" +
      req.params.id +
      " " +
      req.body.name +
      " " +
      req.body.price
  );
  const id_product = parseInt(req.params.id);
  const result = await Product.update(
    {
      name_product: req.body.name,
      price_product: req.body.price,
    },
    { where: { id_product: id_product } }
  );
  if (result !== null) {
    Product.findAll().then((res) => {
      console.log("guardo en cache");
      client.set(products, JSON.stringify(res), "EX", 60 * 60 * 24);
    });
  }
  return result;
};

const deleteProduct = async (req, products) => {
  const id_product = parseInt(req.params.id);
  const result = await Product.destroy({
    where: { id_product: id_product },
  });
  if (result !== null) {
    Product.findAll().then((res) => {
      console.log("guardo en cache");
      client.set(products, JSON.stringify(res), "EX", 60 * 60 * 24);
    });
  }
  return result;
};

module.exports = {
  listProduct,
  addProduct,
  modifyProduct,
  deleteProduct,
};
