require("dotenv").config();
const bcrypt = require("bcrypt");
const { Op } = require("sequelize");
const User = require("../models/users.model");
const Role = require("../models/roles.model");

const addUser = async (req) => {
  const validate = await User.findOne({
    where: {
      id_role: 1,
      state_user: true,
    },
  });
  const roleSelective = validate == null ? 2 : 2; /* req.body.role */
  console.log("validate " + validate == null);
  console.log("role " + roleSelective);

  const userValidate = await User.findOne({
    where: {
      [Op.or]: [
        { username_user: req.body.username },
        { email_user: req.body.email },
      ],
    },
  });
  if (userValidate == null) {
    const newUser = await User.build({
      username_user: req.body.username,
      namelastname_user: req.body.namelastname,
      email_user: req.body.email,
      phone_user: req.body.phone,
      address_user: req.body.address,
      password_user: req.body.password,
      state_user: false,
      active_user: true,
      id_role: roleSelective,
    });
    bcryptedPassword = await bcrypt.hash(newUser.password_user, 10);
    newUser.password_user = bcryptedPassword;

    const result = await newUser.save();
    return result;
  } else {
    throw "User already exists";
  }
};

const listUser = async () => {
  const adminuser = await User.findOne({
    where: {
      state_user: true,
    },
  });
  if (adminuser.id_role == 1) {
    const result = await User.findAll({
      include: {
        model: Role,
        attributes: ["name_role"],
      },
    });
    return result;
  } else {
    const result = await User.findAll({
      where: {
        id_user: adminuser.id_user,
      },
      include: {
        model: Role,
        attributes: ["name_role"],
      },
    });
    return result;
  }
};

const modifyUser = async (req) => {
  const id_user = parseInt(req.params.id);
  const userupdate = await User.findOne({ where: { id_user: id_user } });
  const password = userupdate.password_user;

  if (typeof req.body.password != "undefined") {
    bcryptedPassword = await bcrypt.hash(req.body.password, 10);
  }
  //bcryptedPassword = await bcrypt.hash(req.body.password, 10);

  const result = await User.update(
    {
      username_user: req.body.username,
      namelastname_user: req.body.namelastname,
      email_user: req.body.email,
      phone_user: req.body.phone,
      address_user: req.body.address,
      password_user:
        typeof req.body.password != "undefined" ? bcryptedPassword : password,
      state_user: false,
      active_user: true,
      id_role: req.body.role,
    },
    { where: { id_user: id_user } }
  );
  return result;
};

const disabledUser = async (req) => {
  const id_user = parseInt(req.params.id);
  const result = await User.update(
    {
      active_user: req.body.active,
    },
    { where: { id_user: id_user } }
  );
  return result;
};

const deleteUser = async (req) => {
  const id_user = parseInt(req.params.id);
  const result = await User.destroy({
    where: { id_user: id_user },
  });
  return result;
};

const loginId = async () => {
  const indexUser = await User.findOne({
    where: {
      id_role: 2,
      state_user: true,
    },
  });
  const indexAdmin = await User.findOne({
    where: {
      id_role: 1,
      state_user: true,
    },
  });
  console.log("indexuser " + indexUser + "indexAdmin " + indexAdmin);
  if (indexUser >= 0) {
    return indexUser;
  } else if (indexAdmin >= 0) {
    return indexAdmin;
  } else {
    return "none";
  }
};

module.exports = {
  listUser,
  addUser,
  modifyUser,
  deleteUser,
  disabledUser,
  loginId,
};
