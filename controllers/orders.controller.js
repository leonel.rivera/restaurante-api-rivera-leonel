require("dotenv").config();
let Order = require("../models/orders.model");
const State = require("../models/states.model");
const Payment = require("../models/payments.model");
let Product = require("../models/products.model");
let OrderProduct = require("../models/order_product.model");
let User = require("../models/users.model");
let userController = require("./users.controller");

const listOrderAdmin = async () =>
  await Order.findAll({
    include: [
      {
        model: State,
        attributes: ["name_state"],
      },
      {
        model: Payment,
        attributes: ["name_payment"],
      },
      {
        model: User,
        attributes: ["username_user", "address_user"],
      },
      {
        model: Product,
        attributes: ["id_product", "name_product"],
        through: {
          attributes: [
            "quantity_order_product",
            "totalproductprice_order_product",
          ],
        },
      },
    ],
  });

const listOrderUser = async () =>
  await Order.findAll({
    include: [
      {
        model: State,
        attributes: ["name_state"],
      },
      {
        model: Payment,
        attributes: ["name_payment"],
      },
      {
        model: User,
        attributes: ["username_user", "address_user"],
        where: {
          state_user: true,
        },
      },
      {
        model: Product,
        attributes: ["id_product", "name_product"],
        through: {
          attributes: [
            "quantity_order_product",
            "totalproductprice_order_product",
          ],
        },
      },
    ],
  });

const addOrder = async (req) => {
  const validate = await User.findOne({
    where: {
      state_user: true,
    },
  });
  let time = new Date();
  const newOrder = await Order.build({
    id_state: 1,
    time_order: time.toLocaleString("en-US"),
    id_payment: req.body.payment,
    totaltopay_order: 0,
    id_user: validate.id_user,
    address_order: req.body.address,
  });

  const result = await newOrder.save();
  return result;
};

const addOrderProduct = async (req) => {
  const orderTotal = await Order.findOne({
    where: {
      id_order: parseInt(req.params.idorder),
    },
  });

  const newOrderProduct = await OrderProduct.build({
    id_order: parseInt(req.params.idorder),
    id_product: parseInt(req.params.idproduct),
    quantity_order_product: parseInt(req.body.quantity),
    totalproductprice_order_product: await totalPrice(
      parseInt(req.params.idproduct),
      parseInt(req.body.quantity)
    ),
  });
  const result = await newOrderProduct.save();
  /* 
    if (orderTotal !== null) {
        Order.update({
            totaltopay_order: orderTotal.totaltopay_order + await totalPrice(parseInt(req.params.idproduct), parseInt(req.body.quantity))
        }, {
            where: {
                id_order: parseInt(req.params.idorder)
            }
        });
    } */

  return result;
};

const updatetotalprice_order = async (req) => {
  return await OrderProduct.findAll({
    where: { id_order: parseInt(req.params.idorder) },
  }).then((resultado) => {
    if (resultado) {
      let total = 0;
      resultado.forEach((element) => {
        total += element.totalproductprice_order_product;
      });
      return Order.update(
        {
          totaltopay_order: total,
        },
        { where: { id_order: parseInt(req.params.idorder) } }
      );
    }
  });
};

const modifyOrderAdmin = async (req) => {
  const id_order = parseInt(req.params.id);
  const result = await Order.findOne({
    where: {
      id_order: id_order,
    },
  });
  if (result !== null) {
    result.id_state = parseInt(req.body.state);
    await result.save();
  }
  return "Order modified";
};

const modifyOrderUser = async (req) => {
  const orderTotal = await Order.findOne({
    where: {
      id_order: parseInt(req.params.idorder),
      id_state: 1,
    },
  });
  const orderProductTotal = await OrderProduct.findOne({
    where: {
      id_order: parseInt(req.params.idorder),
      id_product: parseInt(req.params.idproduct),
    },
  });
  const totalPriceProduct = orderProductTotal.totalproductprice_order_product;

  if (orderTotal !== null) {
    const result = await OrderProduct.update(
      {
        quantity_order_product: parseInt(req.body.quantity),
        totalproductprice_order_product: await totalPrice(
          parseInt(req.params.idproduct),
          parseInt(req.body.quantity)
        ),
      },
      {
        where: {
          id_order: parseInt(req.params.idorder),
          id_product: parseInt(req.params.idproduct),
        },
      }
    );

    Order.update(
      {
        totaltopay_order:
          orderTotal.totaltopay_order -
          totalPriceProduct +
          (await totalPrice(
            parseInt(req.params.idproduct),
            parseInt(req.body.quantity)
          )),
      },
      {
        where: {
          id_order: parseInt(req.params.idorder),
        },
      }
    );
    //}
    return result;
  } else {
    return (result = "Order cannot be modified. It is in a closed state");
  }
};

const modifyOrderUserConfirm = async (req) => {
  const id_order = parseInt(req.params.id);
  const result = await Order.findOne({
    where: {
      id_order: id_order,
      id_state: 1,
    },
  });
  if (result !== null) {
    result.id_state = 2;
    await result.save();
  }
  return "Order modified";
};

const deleteOrder = async (req) => {
  const orderTotal = await Order.findOne({
    where: {
      id_order: parseInt(req.params.idorder),
      id_state: 1,
    },
  });
  const orderProductTotal = await OrderProduct.findOne({
    where: {
      id_order: parseInt(req.params.idorder),
      id_product: parseInt(req.params.idproduct),
    },
  });
  const totalPriceProduct = orderProductTotal.totalproductprice_order_product;

  if (orderTotal !== null) {
    const result = await OrderProduct.destroy({
      where: {
        id_order: parseInt(req.params.idorder),
        id_product: parseInt(req.params.idproduct),
      },
    });

    Order.update(
      {
        totaltopay_order: orderTotal.totaltopay_order - totalPriceProduct,
      },
      {
        where: {
          id_order: parseInt(req.params.idorder),
        },
      }
    );

    return result;
  } else {
    return (result = "Order cannot be modified. It is in a closed state");
  }
};

const totalPrice = async (product, quantity) => {
  const findProduct = await Product.findOne({
    where: {
      id_product: product,
    },
  });
  const result = findProduct.price_product * quantity;
  return result;
};

module.exports = {
  listOrderAdmin,
  listOrderUser,
  addOrder,
  modifyOrderAdmin,
  modifyOrderUser,
  modifyOrderUserConfirm,
  deleteOrder,
  addOrderProduct,
  updatetotalprice_order,
};
