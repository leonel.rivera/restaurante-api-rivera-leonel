require("dotenv").config();
const Address = require('../models/addresses.model');


const addAddress = async (req) => {
    const newAddress = await Address.build({
        name_address: req.body.name_address
    });

    const result = await newAddress.save();
    return result;
}

const listAddress = async () => await Address.findAll();

const modifyAddress = async (req) => {
    const id_address = parseInt(req.params.id);
    const result = await Address.update({
        name_address: req.body.name_address
    },
    { where: { id_address: id_address } }
    );
    return result;
}

const deleteAddress = async (req) => {
    const id_address = parseInt(req.params.id);
    const result = await Address.destroy({
        where: { id_address: id_address }
    });
    return result;
}

module.exports = {
    listAddress,
    addAddress,
    modifyAddress,
    deleteAddress
}
