const express = require("express");
const app = express();
const helmet = require("helmet");
const usersRoutes = require("./routes/users.route");
const productRoutes = require("./routes/products.route");
const orderRoute = require("./routes/orders.route");
const paymentRoutes = require("./routes/payments.route");
const stateRoutes = require("./routes/states.route");
const roleRoutes = require("./routes/roles.route");
const addressRoutes = require("./routes/addresses.route");
const paygatewayRoutes = require("./routes/paygateway.route");
//const redirectRoutes = require("./routes/redirects.route");
//const host = 'http://localhost:';
const port = 4000;
const sequelize = require("./config/db.config");
let userMiddleware = require("./middlewares/users.middleware");
const cors = require("cors");
require("./models/asociations");
//require('./seeds/seed');
//auth0
const passportSetup = require("./config/passport-setup");

const swaggerUI = require("swagger-ui-express");
const swaggerJSDoc = require("swagger-jsdoc");
const router = require("./routes/addresses.route");

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "API - Restaurant",
      version: "1.0.0",
    },
  },
  apis: [
    "./index.js",
    "./routes/users.route.js",
    "./routes/products.route.js",
    "./routes/orders.route.js",
    "./routes/payments.route.js",
    "./routes/states.route.js",
    "./routes/roles.route.js",
    "./routes/addresses.route.js",
    "./routes/paygateway.route.js",
    /* "./routes/redirect.route.js", */
  ],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use("/api-docs/", swaggerUI.serve, swaggerUI.setup(swaggerDocs));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

/**
 *  @swagger
 *  info:
 *    title: 'Restaurant'
 */

/*  app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});
 */
app.use(
  userMiddleware.cors,
  userMiddleware.loginSate,
  userMiddleware.verifyToken,
  userMiddleware.loginRol
);
app.use(cors());
//app.use("/", redirectRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoute);
app.use("/users", usersRoutes);
app.use("/payments", paymentRoutes);
app.use("/states", stateRoutes);
app.use("/roles", roleRoutes);
app.use("/addresses", addressRoutes);
app.use("/paygateway", paygatewayRoutes);

app.listen(port, () => {
  console.log(`Listening ${port}`);
  sequelize
    .authenticate()
    .then(() => {
      console.log("BBDD conectada");
    })
    .catch((err) => {
      console.log("BBDD error", err);
    });
  sequelize
    .sync({ force: false })
    .then(() => {
      console.log("BBDD sync");
    })
    .catch((err) => {
      console.log("BBDD sync error", err);
    });
});

app.use(helmet());
