const {Model,DataTypes}= require('sequelize');
const sequelize = require("../config/db.config");

class Address extends Model{}
Address.init({
    id_address: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name_address: {
        type: DataTypes.STRING
    }
},{
    sequelize,
    modelName:'addresses',
    timestamps: false
});

module.exports = Address;