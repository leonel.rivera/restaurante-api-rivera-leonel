const {Model,DataTypes}= require('sequelize');
const sequelize = require("../config/db.config");

class Payment extends Model{}
Payment.init({
    id_payment: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name_payment: {
        type: DataTypes.STRING
    }
},{
    sequelize,
    modelName:'payments',
    timestamps: false
});

module.exports = Payment;