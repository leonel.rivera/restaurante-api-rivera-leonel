const User = require('../models/users.model');
const Role = require('../models/roles.model');

const Order = require('../models/orders.model');
const State = require('../models/states.model');
const Payment = require('../models/payments.model');

const Product = require('../models/products.model');
const OrderProduct = require('../models/order_product.model');


Role.hasMany(User,{
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    foreignKey:'id_role'});

User.belongsTo(Role,{foreignKey:'id_role'});

State.hasMany(Order,{
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    foreignKey:'id_state'});
Order.belongsTo(State,{foreignKey:'id_state'});

Payment.hasMany(Order,{
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    foreignKey:'id_payment'});
Order.belongsTo(Payment,{foreignKey:'id_payment'});

User.hasMany(Order,{
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    foreignKey:'id_user'});
Order.belongsTo(User,{foreignKey:'id_user'});

Order.belongsToMany(Product,{
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    through: OrderProduct,
    foreignKey:'id_order'
});
Product.belongsToMany(Order,{
    onDelete: 'RESTRICT',
    onUpdate: 'RESTRICT',
    through: OrderProduct,
    foreignKey:'id_product'
});