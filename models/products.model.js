const {Model,DataTypes}= require('sequelize');
const sequelize = require("../config/db.config");

class Product extends Model{}
Product.init({
    id_product: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name_product: {
        type: DataTypes.STRING
    },
    price_product: {
        type: DataTypes.INTEGER
    }
},{
    sequelize,
    modelName:'products',
    timestamps: false
});

module.exports = Product;