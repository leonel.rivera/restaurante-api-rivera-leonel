const {Model,DataTypes}= require('sequelize');
const sequelize = require("../config/db.config");

class OrderProduct extends Model{}
OrderProduct.init({
    id_order_product: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    id_order: {
        type: DataTypes.INTEGER
    },
    id_product: {
        type: DataTypes.INTEGER
    },
    quantity_order_product: {
        type: DataTypes.INTEGER
    },
    totalproductprice_order_product : {
        type: DataTypes.INTEGER
    }
},{
    sequelize,
    modelName:'order_product',
    timestamps: false
});

module.exports = OrderProduct;