const {Model,DataTypes}= require('sequelize');
const sequelize = require("../config/db.config");

class State extends Model{}
State.init({
    id_state: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name_state: {
        type: DataTypes.STRING
    }
},{
    sequelize,
    modelName:'states',
    timestamps: false
});

module.exports = State;