const {Model,DataTypes}= require('sequelize');
const sequelize = require("../config/db.config");

class User extends Model{}
User.init({
    id_user: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username_user: {
        type: DataTypes.STRING,
    },
    namelastname_user: {
        type: DataTypes.STRING,
    },
    email_user: {
        type: DataTypes.STRING,
    },
    phone_user: {
        type: DataTypes.INTEGER,
    },
    address_user: {
        type: DataTypes.STRING,
    },
    password_user: {
        type: DataTypes.STRING,
    },
    state_user: {
        type: DataTypes.BOOLEAN,
    },
    active_user: {
        type: DataTypes.BOOLEAN,
    },
    id_role: {
        type: DataTypes.INTEGER,
    },
    id_idp: {
        type: DataTypes.STRING,
    }
},{
    sequelize,
    modelName:'users',
    timestamps: false
});

const token='';
module.exports = User;