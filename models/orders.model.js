const {Model,DataTypes}= require('sequelize');
const sequelize = require("../config/db.config");

class Order extends Model{}
Order.init({
    id_order: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    id_state: {
        type: DataTypes.INTEGER,
    },
    time_order: {
        type: DataTypes.DATE,
    },
    id_payment: {
        type: DataTypes.INTEGER,
    },
    totaltopay_order: {
        type: DataTypes.INTEGER,
    },
    id_user: {
        type: DataTypes.INTEGER,
    },
    //id_address: {
    address_order: {    
        type: DataTypes.STRING,
    }
},{
    sequelize,
    modelName:'orders',
    timestamps: false
});

module.exports = Order;