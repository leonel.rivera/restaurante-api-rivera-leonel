const {Model,DataTypes}= require('sequelize');
const sequelize = require("../config/db.config");

class Role extends Model{}
Role.init({
    id_role: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name_role: {
        type: DataTypes.STRING
    }
},{
    sequelize,
    modelName:'roles',
    timestamps: false
});

module.exports = Role;