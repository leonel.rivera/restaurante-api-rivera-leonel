#!/bin/sh

echo Step 1: Update System
sudo apt -y update

echo Step 2: Install basic dependencies
sudo apt -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

echo Step 3: Install Docker CE on Ubuntu 22.04|20.04|18.04
sudo apt remove docker docker.io containerd runc -y
echo Step 4
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker-archive-keyring.gpg
echo Step 5
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
echo Step 6
sudo apt update -y
echo Step 7
sudo apt install docker-ce docker-ce-cli containerd.io -y
echo Docker Compose
echo Step 10 
sudo apt update -y
echo Step 11
sudo apt install -y curl wget
echo Step 12
curl -s https://api.github.com/repos/docker/compose/releases/latest | grep browser_download_url  | grep docker-compose-linux-x86_64 | cut -d '"' -f 4 | wget -qi -
echo Step 13
chmod +x docker-compose-linux-x86_64
echo Step 14
sudo mv docker-compose-linux-x86_64 /usr/local/bin/docker-compose
echo Step 15
echo bash completion
echo Step 16 
sudo curl -L https://raw.githubusercontent.com/docker/compose/master/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
echo Step 17
source /etc/bash_completion.d/docker-compose
echo Step 8
sudo usermod -aG docker $USER
echo Step 9
newgrp docker