require('dotenv').config();
const { AssertionError } = require('assert');
const assert = require('assert');
const fetch = require('node-fetch');

describe(`TEST APIs`, () => {

    it(`POST/users/register`, async () => {
        const url = `http://localhost:4000/users/register`;
        const data = {
            username: 'test',
            namelastname: 'test',
            email: 'test@mail.com',
            phone: '123456789',
            address: 'test',
            password: 'test'
        };
        await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                assert.equal(response.status, 200);
            })
            .catch(response => {
                assert.throws(() => {
                    assert.equal(response.status, 400);
            });
        });
    });

    it(`POST/users/login`, async () => {
        const url = `http://localhost:4000/users/login`;
        const data = {
            username: 'test',
            password: 'test'
        };
        await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                assert.equal(response.status, 200);
            });
    });

    it(`POST/users/logout`, async () => {
        const url = `http://localhost:4000/users/logout`;
        const data = {
            token: 'test'
        };
        await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                assert.equal(response.status, 200);
            });
    });

});