const express = require("express");
const router = express.Router();
let userController = require("../controllers/users.controller");
let userMiddleware = require("../middlewares/users.middleware");

const passport = require("passport");
const passportSetup = require("../config/passport-setup");

/**
 * @swagger
 *  tags:
 *  - name: 'users'
 *    description: 'Operations about user'
 * /users/list:
 *  get:
 *    tags:
 *    - users
 *    summary: list all users by user or admin
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/list", (req, res) => {
  userController
    .listUser()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /users:
 *  post:
 *    tags:
 *    - users
 *    summary: create user
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: username
 *      description: Username
 *      in: formData
 *      required: true
 *      type: string
 *    - name: namelastname
 *      description: Name and Lastname user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: phone
 *      description: Phone user
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: address
 *      description: Address user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Password user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: role
 *      description: Role user
 *      in: formData
 *      required: true
 *      type: integer
 *      schema:
 *        example: 1 - admin or 2 - user
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/", (req, res) => {
  userController
    .addUser(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /users/register:
 *  post:
 *    tags:
 *    - users
 *    summary: create user
 *    parameters:
 *    - name: username
 *      description: Username
 *      in: formData
 *      required: true
 *      type: string
 *    - name: namelastname
 *      description: Name and Lastname user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: phone
 *      description: Phone user
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: address
 *      description: Address user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Password user
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/register", (req, res) => {
  userController
    .addUser(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /users/modificar/{id}:
 *  put:
 *    tags:
 *    - users
 *    summary: modify user
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id user
 *      in: path
 *      required: false
 *      type: integer
 *    - name: username
 *      description: Username
 *      in: formData
 *      required: false
 *      type: string
 *    - name: namelastname
 *      description: Name and Lastname user
 *      in: formData
 *      required: false
 *      type: string
 *    - name: email
 *      description: Email user
 *      in: formData
 *      required: false
 *      type: string
 *    - name: phone
 *      description: Phone user
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: address
 *      description: Address user
 *      in: formData
 *      required: false
 *      type: string
 *    - name: password
 *      description: Password user
 *      in: formData
 *      required: false
 *      type: string
 *    - name: role
 *      description: Role user
 *      in: formData
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.put("/modificar/:id", (req, res) => {
  userController
    .modifyUser(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Update data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /users/disabled/{id}:
 *  put:
 *    tags:
 *    - users
 *    summary: disabled user
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id user
 *      in: path
 *      required: true
 *      type: integer
 *    - name: active
 *      description: Disabled/Enabled user
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 */
router.put("/disabled/:id", (req, res) => {
  userController
    .disabledUser(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Update data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /users/{id}:
 *  delete:
 *    tags:
 *    - users
 *    summary: delete user by id
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id user
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete("/:id", (req, res) => {
  userController
    .deleteUser(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /users/login:
 *  post:
 *    tags:
 *    - users
 *    summary: login user
 *    parameters:
 *    - name: username
 *      description: Username
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Password user
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/login", userMiddleware.validateLogin, (req, res) => {});

/**
 * @swagger
 * /users/logout:
 *  post:
 *    tags:
 *    - users
 *    summary: logout user
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/logout", userMiddleware.validateLogout, (req, res) => {});

router.get(
  "/google",
  passport.authenticate("google", {
    scope: ["profile"],
  })
);

router.get(
  "/google/redirect",
  passport.authenticate("google"),
  userMiddleware.validateLoginidp,
  (req, res) => {}
);

router.get("/facebook", passport.authenticate("facebook"));

router.get(
  "/facebook/redirect",
  passport.authenticate("facebook"),
  userMiddleware.validateLoginidp,
  (req, res) => {}
);

router.get("/linkedin", passport.authenticate("linkedin"));

router.get(
  "/linkedin/redirect",
  passport.authenticate("linkedin"),
  userMiddleware.validateLoginidp,
  (req, res) => {}
);

module.exports = router;
