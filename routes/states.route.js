const express = require("express");
const router = express.Router();
let State = require("../models/states.model");
let stateController = require("../controllers/states.controller");

/**
 * @swagger
 *  tags:
 *  - name: 'states'
 *    description: 'CRUD (Create, Read, Update, Delete) about your States'
 * /states:
 *  get:
 *    tags:
 *    - states
 *    summary: list all states the orders
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/", (req, res) => {
  stateController
    .listState()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /states:
 *  post:
 *    tags:
 *    - states
 *    summary: create state
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: name_state
 *      description: name state
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/", (req, res) => {
  stateController
    .addState(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /states/{id}:
 *  put:
 *    tags:
 *    - states
 *    summary: modify state
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id state
 *      in: path
 *      required: true
 *      type: integer
 *    - name: name_state
 *      description: name state
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put("/:id", (req, res) => {
  stateController
    .modifyState(req)
    .then((result) => {
      console.log("result" + result);
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Update data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /states/{id}:
 *  delete:
 *    tags:
 *    - states
 *    summary: delete state by id
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id state
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete("/:id", (req, res) => {
  stateController
    .deleteState(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});

module.exports = router;
