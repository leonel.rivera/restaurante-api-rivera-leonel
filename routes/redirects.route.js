const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.redirect('https://www.delilah-resto.gq/index/');
});

module.exports = router;