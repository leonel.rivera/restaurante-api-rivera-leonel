const express = require("express");
const router = express.Router();
let Payment = require("../models/payments.model");
let paymentController = require("../controllers/payments.controller");

/**
 * @swagger
 *  tags:
 *  - name: 'payments'
 *    description: 'CRUD (Create, Read, Update, Delete) about your Payments'
 * /payments:
 *  get:
 *    tags:
 *    - payments
 *    summary: list all payments
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/", (req, res) => {
  paymentController
    .listPayment()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /payments:
 *  post:
 *    tags:
 *    - payments
 *    summary: create payment
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: name_payment
 *      description: name payment
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/", (req, res) => {
  paymentController
    .addPayment(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /payments/{id}:
 *  put:
 *    tags:
 *    - payments
 *    summary: modify payment
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id payment
 *      in: path
 *      required: true
 *      type: integer
 *    - name: name_payment
 *      description: name payment
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put("/:id", (req, res) => {
  paymentController
    .modifyPayment(req)
    .then((result) => {
      console.log("result" + result);
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Update data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /payments/{id}:
 *  delete:
 *    tags:
 *    - payments
 *    summary: delete payment by id
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id payment
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete("/:id", (req, res) => {
  paymentController
    .deletePayment(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});

module.exports = router;
