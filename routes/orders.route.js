const express = require("express");
const router = express.Router();
let Order = require("../models/orders.model");
let orderController = require("../controllers/orders.controller");

/**
 * @swagger
 * /orders/get/admin:
 *  get:
 *    tags:
 *    - orders
 *    summary: list all orders by admin
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/get/admin", (req, res) => {
  orderController
    .listOrderAdmin()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /orders/get/user:
 *  get:
 *    tags:
 *    - orders
 *    summary: list all orders by user
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/get/user", (req, res) => {
  orderController
    .listOrderUser()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /orders:
 *   post:
 *     tags:
 *     - orders
 *     summary: Create order
 *     parameters:
 *     - name: authorization
 *       description: User token - You can copy-paste token or detect it automatically
 *       in: header
 *       required: false
 *       type: string
 *     - name: payment
 *       description: Order way to pay
 *       in: formData
 *       required: true
 *       type: integer
 *       schema:
 *         example: 1-Debito, 2-Efectivo, 3-Transf.Bancaria, 4-T.Credito
 *     - name: address
 *       description: Order delivery address
 *       in: formData
 *       required: false
 *       type: string
 *     responses:
 *       '200':
 *         description: Success
 */
router.post("/", (req, res) => {
  orderController
    .addOrder(req)
    .then((result) => {
      console.log("ORDER POST RESULT " + result);
      res.status(200).json({
        status: 200,
        message: "Data find Successfully",
        id: result.id_order,
      });
    })
    .catch((error) => {
      console.log("ORDER POST RESULT " + error);
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /orders/{idorder}/products/{idproduct}:
 *   post:
 *     tags:
 *     - orders
 *     summary: Associate order with products
 *     parameters:
 *     - name: authorization
 *       description: User token - You can copy-paste token or detect it automatically
 *       in: header
 *       required: false
 *       type: string
 *     - name: idorder
 *       description: Order id
 *       in: path
 *       required: true
 *       type: integer
 *     - name: idproduct
 *       description: Product id
 *       in: path
 *       required: true
 *       type: integer
 *     - name: quantity
 *       description: Product quantity
 *       in: formData
 *       required: true
 *       type: integer
 *     responses:
 *       '200':
 *         description: Success
 */
router.post("/:idorder/products/:idproduct", (req, res) => {
  orderController
    .addOrderProduct(req)
    .then(() => {
      orderController.updatetotalprice_order(req);
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: "Order or product does not exist or is already loaded",
        status: 400,
      });
    });
});

/**
 * @swagger
 * /orders/put/admin/{id}:
 *   put:
 *     tags:
 *     - orders
 *     summary: Update order by admin
 *     parameters:
 *     - name: authorization
 *       description: User token - You can copy-paste token or detect it automatically
 *       in: header
 *       required: false
 *       type: string
 *     - name: id
 *       description: Id order
 *       in: path
 *       required: true
 *       type: integer
 *     - name: state
 *       description: State order
 *       in: formData
 *       required: true
 *       type: integer
 *     responses:
 *       '200':
 *         description: Success
 */
router.put("/put/admin/:id", (req, res) => {
  orderController
    .modifyOrderAdmin(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /orders/{idorder}/products/{idproduct}:
 *   put:
 *     tags:
 *     - orders
 *     summary: Update order by user
 *     parameters:
 *     - name: authorization
 *       description: User token - You can copy-paste token or detect it automatically
 *       in: header
 *       required: false
 *       type: string
 *     - name: idorder
 *       description: Order id
 *       in: path
 *       required: true
 *       type: integer
 *     - name: idproduct
 *       description: Product id
 *       in: path
 *       required: true
 *       type: integer
 *     - name: quantity
 *       description: Product quantity
 *       in: formData
 *       required: true
 *       type: integer
 *     responses:
 *       '200':
 *         description: Success
 */
router.put("/:idorder/products/:idproduct", (req, res) => {
  orderController
    .modifyOrderUser(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /orders/put/user/confirm/{id}:
 *   put:
 *     tags:
 *     - orders
 *     summary: Update order state
 *     parameters:
 *     - name: authorization
 *       description: User token - You can copy-paste token or detect it automatically
 *       in: header
 *       required: false
 *       type: string
 *     - name: id
 *       description: Id order
 *       in: path
 *       required: true
 *       type: integer
 *     responses:
 *       '200':
 *         description: Success
 */
router.put("/put/user/confirm/:id", (req, res) => {
  orderController
    .modifyOrderUserConfirm(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 *  tags:
 *  - name: 'orders'
 *    description: Access to Restaurant orders
 * /orders/delete/{idorder}/products/{idproduct}:
 *  delete:
 *    tags:
 *    - orders
 *    summary: Delete order by id
 *    description:
 *    parameters:
 *     - name: authorization
 *       description: User token - You can copy-paste token or detect it automatically
 *       in: header
 *       required: false
 *       type: string
 *     - name: idorder
 *       description: Order id
 *       in: path
 *       required: true
 *       type: integer
 *     - name: idproduct
 *       description: Product id
 *       in: path
 *       required: true
 *       type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete("/delete/:idorder/products/:idproduct", (req, res) => {
  orderController
    .deleteOrder(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

module.exports = router;
