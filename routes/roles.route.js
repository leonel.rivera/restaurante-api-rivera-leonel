const express = require("express");
const router = express.Router();
let Role = require("../models/roles.model");
let roleController = require("../controllers/roles.controller");

/**
 * @swagger
 *  tags:
 *  - name: 'roles'
 *    description: 'CRUD (Create, Read, Update, Delete) about your User Roles'
 * /roles:
 *  get:
 *    tags:
 *    - roles
 *    summary: list all roles
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/", (req, res) => {
  roleController
    .listRole()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /roles:
 *  post:
 *    tags:
 *    - roles
 *    summary: create role
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: name_role
 *      description: name role
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/", (req, res) => {
  roleController
    .addRole(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /roles/{id}:
 *  put:
 *    tags:
 *    - roles
 *    summary: modify role
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id role
 *      in: path
 *      required: true
 *      type: integer
 *    - name: name_role
 *      description: name role
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put("/:id", (req, res) => {
  roleController
    .modifyRole(req)
    .then((result) => {
      console.log("result" + result);
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Update data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /roles/{id}:
 *  delete:
 *    tags:
 *    - roles
 *    summary: delete role by id
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id role
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete("/:id", (req, res) => {
  roleController
    .deleteRole(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});

module.exports = router;
