const express = require("express");
const router = express.Router();
let Address = require("../models/addresses.model");
let addressController = require("../controllers/addresses.controller");

/**
 * @swagger
 *  tags:
 *  - name: 'addresses'
 *    description: 'CRUD (Create, Read, Update, Delete) about your User Address'
 * /addresses:
 *  get:
 *    tags:
 *    - addresses
 *    summary: list all addresses the orders
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/", (req, res) => {
  addressController
    .listAddress()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /addresses:
 *  post:
 *    tags:
 *    - addresses
 *    summary: create address
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: name_address
 *      description: name address
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/", (req, res) => {
  addressController
    .addAddress(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /addresses/{id}:
 *  put:
 *    tags:
 *    - addresses
 *    summary: modify address
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id address
 *      in: path
 *      required: true
 *      type: integer
 *    - name: name_address
 *      description: name address
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put("/:id", (req, res) => {
  addressController
    .modifyAddress(req)
    .then((result) => {
      console.log("result" + result);
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Update data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /addresses/{id}:
 *  delete:
 *    tags:
 *    - addresses
 *    summary: delete address by id
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id address
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete("/:id", (req, res) => {
  addressController
    .deleteAddress(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});

module.exports = router;
