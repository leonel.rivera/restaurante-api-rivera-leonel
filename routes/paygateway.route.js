const express = require("express");
const router = express.Router();
const Order = require("../models/orders.model");
let paygateway = require("../controllers/paygateway.controller");

// Paypal
const request = require("request");

// SDK de Mercado Pago
const mercadopago = require("mercadopago");

// Agrega credenciales
mercadopago.configure({
  access_token: process.env.MERCADO_ACCESS_TOKEN,
});

router.get("/mpago", (req, res) => {
  console.log("mpago req" + JSON.stringify(req.query));
  const totaltopay_order = JSON.parse(req.query.totaltopay_order);
  const id_order = JSON.parse(req.query.id_order);
  console.log("totaltopay_order" + totaltopay_order + "id_order" + id_order);

  const preference = {
    items: [
      {
        //id: req.body.id_payment,
        title: "Pago de orden",
        description: "Pago total de pedido",
        quantity: 1,
        unit_price: totaltopay_order,
      },
    ],
    back_urls: {
      success:
        process.env.API +
        "/paygateway/mpago/redirect" +
        "?id_order=" +
        id_order,
      failure:
        process.env.API +
        "/paygateway/mpago/redirect" +
        "?id_order=" +
        id_order,
      pending:
        process.env.API +
        "/paygateway/mpago/redirect" +
        "?id_order=" +
        id_order,
      /* success: process.env.BACK_URL_SUCCESS,
      failure: process.env.BACK_URL_FAILURE, */
    },
    auto_return: "approved",
    //external_reference: req.body.id_payment,
  };

  mercadopago.preferences
    .create(preference)
    .then(function (response) {
      // En esta instancia deberás asignar el valor dentro de response.body.id por el ID de preferencia solicitado en el siguiente paso
      console.log(response.body);
      res.redirect(response.body.init_point);
    })
    .catch(function (error) {
      console.log(error);
    });
});

router.get("/mpago/redirect", (req, res) => {
  console.log("mpago redirect" + JSON.stringify(req.query));
  Order.update(
    {
      id_state: 2,
    },
    {
      where: {
        id_order: JSON.parse(req.query.id_order),
      },
    }
  )
    .then((result) => {
      console.log("result" + JSON.stringify(result));
      res.status(301).redirect(process.env.API_FRONT);
    })
    .catch((err) => {
      console.log("err" + JSON.stringify(err));
      res.redirect(process.env.API_FRONT);
    });
  //res.status(301).redirect(process.env.API_);
});

/* PAYPAL */
/**
 * 1️⃣ Paso
 * Crear una aplicacion en Ppaypal
 * Aqui agregamos las credenciales de nuestra app de PAYPAL
 * https://developer.paypal.com/developer/applications (Debemos acceder con nuestra cuenta de Paypal)
 * [Cuentas de TEST] https://developer.paypal.com/developer/accounts/
 */

const CLIENT = process.env.PAYPAL_CLIENT_ID;
const SECRET = process.env.PAYPAL_CLIENT_SECRET;
const PAYPAL_API = "https://api-m.sandbox.paypal.com"; // Live https://api-m.paypal.com

const auth = { user: CLIENT, pass: SECRET };

/**
 * Establecemos los contraladores que vamos a usar
 */

const createPayment = (req, res) => {
  console.log(`imprimo req: ${JSON.stringify(req.body)}`);
  const idPedido = req.body.id;
  const body = {
    intent: "CAPTURE",
    purchase_units: [
      {
        amount: {
          currency_code: "USD", //https://developer.paypal.com/docs/api/reference/currency-codes/
          value: req.body.precioPagar,
        },
      },
    ],
    application_context: {
      brand_name: `MiTienda.com`,
      landing_page: "NO_PREFERENCE", // Default, para mas informacion https://developer.paypal.com/docs/api/orders/v2/#definition-order_application_context
      user_action: "PAY_NOW", // Accion para que en paypal muestre el monto del pago
      return_url:
        process.env.API +
        `/paygateway/paypal/execute-payment` +
        "?id_order=" +
        idPedido, // Url despues de realizar el pago
      cancel_url: process.env.API + `/paygateway/paypal/cancel-payment`, // Url despues de realizar el pago
    },
  };
  //https://api-m.sandbox.paypal.com/v2/checkout/orders [POST]

  request.post(
    `${PAYPAL_API}/v2/checkout/orders`,
    {
      auth,
      body,
      json: true,
    },
    (err, response) => {
      if (res.statusCode == 200) {
        res.json(response.body);
      } else {
        console.log(`not found `);
      }
      // res.json(response.body);
    }
  );
};

/**
 * Esta funcion captura el dinero REALMENTE
 * @param {*} req
 * @param {*} res
 */
const executePayment = (req, res) => {
  const idPedido = req.query.id_order;
  const token = req.query.token; //<-----------
  request.post(
    `${PAYPAL_API}/v2/checkout/orders/${token}/capture`,
    {
      auth,
      body: {},
      json: true,
    },
    (err, response) => {
      try {
        // console.log(`estado=${JSON.stringify(response.body)}`);
        console.log(`idPedido = ${idPedido}`);
        Order.update(
          {
            id_state: 2,
          },
          {
            where: {
              id_order: JSON.parse(idPedido),
            },
          }
        )
          .then((result) => {})
          .catch((error) => {
            res.status(400).json({
              message: "Unable to find data",
              errors: error,
              status: 400,
            });
          });
        res.redirect(process.env.API_FRONT);
        //  res.json({ data: response.body });
      } catch (error) {}
    }
  );
};

const cancelPayment = (req, res) => {
  // return to home from
  res.redirect(process.env.API_);
};
/**
 * 2️⃣ Creamos Ruta para generar pagina de CHECKOUT -
 */

//    http://localhost:3000/create-payment [POST]
router.post(`/paypal/create-payment`, createPayment);

/**
 * 3️⃣ Creamos Ruta para luego que el cliente completa el checkout
 * debemos de capturar el dinero!
 */
router.get(`/paypal/execute-payment`, executePayment);

/**
 * 4 Creamos Ruta por si el cliente completa el cancelar
 * debemos de capturar el cancel!
 */
router.get(`/paypal/cancel-payment`, cancelPayment);

// end paypal

module.exports = router;
