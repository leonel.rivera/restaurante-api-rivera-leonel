const express = require("express");
const router = express.Router();
let Product = require("../models/products.model");
let productController = require("../controllers/products.controller");

/**
 * @swagger
 *  tags:
 *  - name: 'products'
 *    description: 'CRUD (Create, Read, Update, Delete) about your Products'
 * /products:
 *  get:
 *     tags:
 *     - products
 *     summary: list all products
 *     parameters:
 *     - name: authorization
 *       description: User token - You can copy-paste token or detect it automatically
 *       in: header
 *       required: false
 *       type: string
 *     responses:
 *       200:
 *         description: Success
 */
router.get("/", (req, res) => {
  const products = Product;
  const result = productController.listProduct(products);

  result
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /products:
 *  post:
 *    tags:
 *    - products
 *    summary: create a new product
 *    description:
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: name
 *      description: name product
 *      in: formData
 *      required: true
 *      type: string
 *    - name: price
 *      description: Price of the product
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/", (req, res) => {
  productController
    .addProduct(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /products/{id}:
 *  put:
 *    tags:
 *    - products
 *    summary: update product
 *    description:
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id product
 *      in: path
 *      required: true
 *      type: integer
 *    - name: name
 *      description: name product
 *      in: formData
 *      required: true
 *      type: string
 *    - name: price
 *      description: Price of the product
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put("/:id", (req, res) => {
  const products = Product;
  const result = productController.modifyProduct(req, products);

  result
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Update data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * /products/{id}:
 *  delete:
 *    tags:
 *    - products
 *    summary: delete product by id
 *    description:
 *    parameters:
 *    - name: authorization
 *      description: User token - You can copy-paste token or detect it automatically
 *      in: header
 *      required: false
 *      type: string
 *    - name: id
 *      description: Id product
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete("/:id", (req, res) => {
  const products = Product;
  const result = productController.deleteProduct(req, products);

  result
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});

module.exports = router;
